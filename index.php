<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>S2: Activity</title>
</head>
<body>
  <h2>Divisibles of Five</h2>
  <?php forLoop(); ?>

  <h2>Array Manipulation</h2>
  <?php array_push($students, "John Doe"); ?>
  <pre>
    <?php print_r($students); ?>
  </pre>
  <p><?php echo count($students); ?></p>
  <?php array_push($students, "Jane Doe"); ?>
  <pre>
    <?php print_r($students); ?>
  </pre>
  <p><?php echo count($students); ?></p>
  <?php array_shift($students); ?>
  <pre>
    <?php print_r($students); ?>
  </pre>
  <p><?php echo count($students); ?></p>
</body>
</html>