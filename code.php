<?php

function forLoop()
{
  for ($count = 0; $count <= 100; $count += 5) {
    /* 
ALTERNATE SOLUTION
if($count % 5 === 0){
  echo $count . ', ';
}
*/
    echo $count . ", ";
  }
}

$students = [];
